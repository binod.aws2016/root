FROM alpine:3.7
ARG user=appuser
ARG group=appuser
ARG uid=1009
ARG gid=1009
RUN groupadd -g ${gid} ${group} && useradd -u ${uid} -g ${group} -s /bin/sh ${user}
USER appuser
ENTRYPOINT ["cat","/tmp/shadow"]
